:: discasset quick runner

:: download the latest data
node download
:: extract assets from css.txt
node extract-css -skip
:: extract assets from cdn out of js.txt
node extract-cdn -skip
:: extract assets from the other main client file
node extract-cdn2 -skip
:: extract svg icons from js.txt for format 1
node extract-svg -skip
:: extract svg icons from js.txt for format 2
node extract-svg2 -skip
:: extract lotties from webpack modules
node extract-lottie